package com.example.cuncis.sqlitedemo2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cuncis.sqlitedemo2.database.DatabaseHelper;

public class CreatePersonalActivity extends AppCompatActivity {

    Cursor cursor;
    DatabaseHelper dbHelper;
    Button btnSave, btnBack;
    EditText text1, text2, text3, text4, text5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_personal);

        dbHelper = new DatabaseHelper(this);
        text1 = findViewById(R.id.editText1);
        text2 = findViewById(R.id.editText2);
        text3 = findViewById(R.id.editText3);
        text4 = findViewById(R.id.editText4);
        text5 = findViewById(R.id.editText5);
        btnSave = findViewById(R.id.button1);
        btnBack = findViewById(R.id.button2);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("INSERT INTO personal(number, name, date, jk, address) VALUES ('" +
                        text1.getText().toString() + "', '" +
                        text2.getText().toString() + "', '" +
                        text3.getText().toString() + "', '" +
                        text4.getText().toString() + "', '" +
                        text5.getText().toString() + "')");
                Toast.makeText(CreatePersonalActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                MainActivity.mainActivity.refreshList();
                finish();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
