package com.example.cuncis.sqlitedemo2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cuncis.sqlitedemo2.database.DatabaseHelper;

public class UpdatePersonalActivity extends AppCompatActivity {

    Cursor cursor;
    DatabaseHelper dbHelper;
    Button btnUpdate, btnBack;
    EditText text1, text2, text3, text4, text5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_personal);

        dbHelper = new DatabaseHelper(this);
        text1 = findViewById(R.id.editText1);
        text2 = findViewById(R.id.editText2);
        text3 = findViewById(R.id.editText3);
        text4 = findViewById(R.id.editText4);
        text5 = findViewById(R.id.editText5);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM personal WHERE name='" +
                getIntent().getStringExtra("name")+"'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            text1.setText(cursor.getString(0).toString());
            text2.setText(cursor.getString(1).toString());
            text3.setText(cursor.getString(2).toString());
            text4.setText(cursor.getString(3).toString());
            text5.setText(cursor.getString(4).toString());
        }
        btnUpdate = findViewById(R.id.button1);
        btnBack = findViewById(R.id.button2);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("UPDATE personal SET name='" +
                        text2.getText().toString() + "', date='" +
                        text3.getText().toString() + "', jk='" +
                        text4.getText().toString() + "', address='" +
                        text5.getText().toString() + "' WHERE number='" +
                        text1.getText().toString() + "'");
                Toast.makeText(UpdatePersonalActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                MainActivity.mainActivity.refreshList();
                finish();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
