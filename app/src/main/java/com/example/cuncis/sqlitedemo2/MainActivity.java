package com.example.cuncis.sqlitedemo2;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cuncis.sqlitedemo2.database.DatabaseHelper;

public class MainActivity extends AppCompatActivity {

    String register[];
    ListView listData;
    Menu menu;
    Cursor cursor;
    DatabaseHelper dbCenter;
    public static MainActivity mainActivity;
    Button btnAddPersonal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAddPersonal = findViewById(R.id.btn_addData);
        btnAddPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CreatePersonalActivity.class);
                startActivity(intent);
            }
        });

        mainActivity = this;
        dbCenter = new DatabaseHelper(this);
        refreshList();

    }

    public void refreshList() {
        SQLiteDatabase db = dbCenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM personal", null);
        register = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i=0; i<cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            register[i] = cursor.getString(i).toString();
        }
        listData = findViewById(R.id.lv_data);
        listData.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, register));
        listData.setSelected(true);
        listData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, final long id) {
                final String selection = register[position];
                final CharSequence[] dialogItem = { "Display Personal", "Update Personal", "Delete Personal" };
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Choose");
                builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent i = new Intent(getApplicationContext(), DisplayPersonalActivity.class);
                                i.putExtra("name", selection);
                                startActivity(i);
                                break;
                            case 1:
                                i = new Intent(getApplicationContext(), UpdatePersonalActivity.class);
                                i.putExtra("name", selection);
                                startActivity(i);
                                break;
                            case 2:
                                SQLiteDatabase db = dbCenter.getWritableDatabase();
                                db.execSQL("DELETE FROM personal WHERE name='"+ selection +"'");
                                refreshList();
                                break;
                            default:
                                Toast.makeText(MainActivity.this, "That was Happened", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });
//        ((ArrayAdapter)listData.getAdapter()).notifyDataSetInvalidated();
    }

}















